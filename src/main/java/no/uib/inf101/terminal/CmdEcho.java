package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String eksempel = "";
        for (String word: args){
            eksempel += word + " ";
        }
        return eksempel;
    }

    @Override
    public String getName() {
        return "echo"; 
    }

}